package cmd_generate_releasenote

import (
	"bytes"
	"fmt"
	"showmeyourcode/changelog-releasenotes-generator/src/command"
	"showmeyourcode/changelog-releasenotes-generator/src/constant"
	"showmeyourcode/changelog-releasenotes-generator/src/custom_io_tool"
	"showmeyourcode/changelog-releasenotes-generator/src/json_helper"
	"showmeyourcode/changelog-releasenotes-generator/src/path_finder"
	"time"
)

type CommandGenerateReleaseNote struct {
	FileName string
}

func (c *CommandGenerateReleaseNote) Execute(workingDirectory string) {
	fmt.Println("Executing a command with source: " + workingDirectory)

	var releaseNoteSource = path_finder.GetReleaseNoteFolderPath(workingDirectory)
	var folder = custom_io_tool.GetFolderContent(releaseNoteSource)

	fmt.Printf("Release notes folder content: %s\n", folder.Files)
	fmt.Println("### The command is going to replace the old release notes file completely ###")

	details, err := json_helper.ParseJsonObject(path_finder.CombinePath(path_finder.GetReleaseNoteFolderPath(workingDirectory), constant.ReleaseNotestDetails))

	if err != nil {
		command.StopExecutionIfError(err, fmt.Sprintf("Cannot load %s.", constant.ReleaseNotestDetails))
	}

	var buffer bytes.Buffer

	buffer.WriteString(constant.ReleaseNotestHeader + "\n\n")

	buffer.WriteString(details["description"] + "\n\n")

	buffer.WriteString(fmt.Sprintf("## ?.?.? (%s)\n\n", time.Now().Format("2006-1-2")))

	buffer.WriteString("Duis vitae sapien nec nisi egestas pellentesque. Nullam quis mattis libero. Vestibulum aliquam aliquam metus in vulputate. Integer egestas augue sit amet diam aliquet, non fermentum erat semper. Fusce sodales facilisis enim, sed tristique tortor dictum vel. Nulla pretium auctor malesuada. Cras pharetra eu dui at pharetra. Duis non maximus lacus. Mauris eget ante in ipsum laoreet luctus ullamcorper vitae nisi. Sed at vehicula orci. Duis feugiat venenatis elit, sed commodo libero porttitor eget. Nunc vel tellus vel dui accumsan rhoncus. Donec venenatis felis eu porttitor consectetur. Aenean id bibendum justo. Donec vel eleifend lacus.\n\n")
	buffer.WriteString("Duis maximus, velit in pellentesque semper, velit mi condimentum dui, sit amet commodo ex nulla et est. Integer a fringilla sapien, a porta tortor. Nullam pellentesque tellus ut elit aliquam feugiat. Sed sit amet pretium ligula. Ut id dictum nibh. In non nunc lacus. Nullam eu laoreet libero, in vehicula erat. Cras a ultrices orci. Mauris tempor augue tristique purus consequat semper. Vestibulum sodales mi ut urna scelerisque, ut mattis arcu fringilla. Praesent justo arcu, imperdiet a nunc at, feugiat finibus nisl. Vestibulum a gravida risus. Aenean vulputate sit amet justo molestie condimentum. Donec eu sodales magna. Aenean imperdiet vel lacus eget convallis. Praesent feugiat euismod urna, id semper neque dignissim eget.\n\n")

	err = custom_io_tool.SaveFile(c.FileName, buffer.String())
	if err != nil {
		command.StopExecutionIfError(err, "Cannot save the release notes.")
	}
}

func (c *CommandGenerateReleaseNote) PrintInfo() {
	fmt.Println("Finish preparing the latest release notes document!")
}
