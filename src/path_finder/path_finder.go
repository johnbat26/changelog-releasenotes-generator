package path_finder

import (
	"log"
	"os"
	"path/filepath"
	"showmeyourcode/changelog-releasenotes-generator/src/constant"
)

func GetPathLastElement(path string) (rootPath string, lastElement string) {
	return filepath.Split(path)
}

func CombinePath(path string, element string) string {
	return filepath.Join(path, element)
}

func GetReleaseNoteFolderPath(workingDirectoryPath string) string {
	return CombinePath(workingDirectoryPath, addSuffixFromSourceDirectory(constant.ReleaseNoteDirectory))
}

func GetWorkingDirectoryPath() string {
	dir, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}
	return dir
}

func addSuffixFromSourceDirectory(directoryName string) string {
	return CombinePath("source", directoryName)
}
