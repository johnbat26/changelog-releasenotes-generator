package json_helper

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestParseJsonObject(t *testing.T) {
	const path = "./test-json/test-object.json"
	var object, _ = ParseJsonObject(path)
	assert.NotNil(t, object)
}

func TestParseJsonArray(t *testing.T) {
	const path = "./test-json/test-array.json"
	var array, _ = ParseJsonArray(path)
	assert.NotNil(t, array)
}
