package json_helper

import (
	"encoding/json"
	"showmeyourcode/changelog-releasenotes-generator/src/custom_io_tool"
)

func ParseJsonObject(path string) (map[string]string, error) {
	var data map[string]string
	err := json.Unmarshal([]byte(custom_io_tool.LoadFileContent(path)), &data)
	return data, err
}

func ParseJsonArray(path string) ([]map[string]string, error) {
	var data []map[string]string
	err := json.Unmarshal([]byte(custom_io_tool.LoadFileContent(path)), &data)
	return data, err
}
