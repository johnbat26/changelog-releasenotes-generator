module showmeyourcode/changelog-releasenotes-generator

go 1.16

require (
	github.com/stretchr/objx v0.3.0 // indirect
	github.com/stretchr/testify v1.7.0 // indirect
	github.com/yosssi/gohtml v0.0.0-20201013000340-ee4748c638f4
	golang.org/x/net v0.0.0-20210813160813-60bc85c4be6d // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)
