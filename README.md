# Changelog & Release Notes Generator in Go

| Branch |                                                                                                        Pipeline                                                                                                        |                                                                                                                                                                                                          Code coverage |                                                    Test report                                                    |
|--------|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|:-----------------------------------------------------------------------------------------------------------------:|
| master | [![pipeline status](https://gitlab.com/ShowMeYourCodeYouTube/changelog-releasenotes-generator/badges/master/pipeline.svg)](https://gitlab.com/ShowMeYourCodeYouTube/changelog-releasenotes-generator/-/commits/master) | [![coverage report](https://gitlab.com/ShowMeYourCodeYouTube/changelog-releasenotes-generator/badges/master/coverage.svg)](https://gitlab.com/ShowMeYourCodeYouTube/changelog-releasenotes-generator/-/commits/master) | [link](https://showmeyourcodeyoutube.gitlab.io/changelog-releasenotes-generator/test-report/coverage-report.html) |

A simple tool which can be used to generate documents in Markdown format.    
It was written in order to get familiar with Go language.

## Technology

- Go
- Command pattern

## Project Setup

The project uses modules, it was set up using commands:
- `go mod init showmeyourcode/changelog-releasenotes-generator`
- `go mod tidy`

The preferred IDE is GoLand or IntelliJ with Go plugin because you can easily configure environments and install necessary modules. 

**The program does not create any Git tags. In order to play with it, create some random tags manually.**

## Project structure

The entrypoint of an application is `main.go`.

## Release note

**Release notes are a set of documents delivered to customers with the intent to provide a verbose description of the release of a new version of a product or service.**
These artifacts are generally created by a marketing team or product owner and contain feature summaries, bug fixes, use cases, and other support material.
The release notes are used as a quick guide to what changed outside of the user documentation.  

*Remember*
1. Do not include minor changes (such changes might be included in changelog).
1. Write release notes entries in the past tense.
1. Omit phrases first person like "I have fixed", or references to the pull request like "this pull request fixes".

How to write release notes? https://www.appcues.com/blog/release-notes-examples

Examples:
- https://www.mkdocs.org/about/release-notes/
- https://github.com/openservicebrokerapi/servicebroker/blob/master/release-notes.md
- https://learn.microsoft.com/en-us/visualstudio/releases/2022/release-notes
- https://slack.com/intl/en-au/release-notes/windows

![img](examples/release-notes/service-broker.png)

## Changelog

**A changelog is a log or record of all notable changes made to a project - new features, enhancements, bugs, and other changes in reverse chronological order.**
Changelogs usually link to specific issues or feature requests within a change management system and also may include links to the developer who supplied the change.

The template you can find here: `examples/changelogs/template.md`.

Examples:
- https://github.com/angular/angular/blob/main/CHANGELOG.md
- https://github.com/facebook/react/blob/main/CHANGELOG.md
- https://froala.com/wysiwyg-editor/changelog/

![img](examples/changelogs/vue-cli.png)
