package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	"showmeyourcode/changelog-releasenotes-generator/src/command"
	"showmeyourcode/changelog-releasenotes-generator/src/command/cmd_generate_changelog"
	"showmeyourcode/changelog-releasenotes-generator/src/command/cmd_generate_releasenote"
	"showmeyourcode/changelog-releasenotes-generator/src/constant"
	"showmeyourcode/changelog-releasenotes-generator/src/path_finder"
	"strconv"
	"strings"
)

var digitsOnlyRegex = regexp.MustCompile(`[^0-9]+`)

func main() {
	options := []string{"Release note", "Changelog"}
	reader := bufio.NewReader(os.Stdin)
	fmt.Printf("Welcome in the documentation tool for <PLATFORM-NAME>! Current version: %s\n", constant.Version)
	fmt.Println("If you want to exit the program, please press '0'.")
	fmt.Println("\nSelect an action:")
	for index, option := range options {
		fmt.Printf("%d. %s\n", index+1, option)
	}
	isActionChosen := false
	var sourcePath = ""
	var commandToExecute command.Command = nil
chooseActionLoop:
	for !isActionChosen {
		userChoice, _ := reader.ReadString('\n')
		userChoice = digitsOnlyRegex.ReplaceAllString(strings.TrimSuffix(userChoice, "\n"), "")
		choiceParsed, _ := strconv.Atoi(userChoice)
		switch choiceParsed {
		case 0:
			fmt.Println("Exit program!")
			break chooseActionLoop
		case 1:
			commandToExecute = &cmd_generate_releasenote.CommandGenerateReleaseNote{FileName: constant.ReleaseNotesFileName}
			sourcePath = path_finder.GetWorkingDirectoryPath()
			isActionChosen = true
		case 2:
			commandToExecute = &cmd_generate_changelog.CommandGenerateChangelog{FileName: constant.ChangelogFileName}
			sourcePath = path_finder.GetWorkingDirectoryPath()
			isActionChosen = true
		default:
			log.Fatal("Action not supported!")
		}
	}
	commandToExecute.Execute(sourcePath)
	commandToExecute.PrintInfo()
}
